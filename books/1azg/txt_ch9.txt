
Der Schweiß hatte sein T-Shirt durchweicht, und jeder Atemzug schmerzte, aber Nik rannte weiter auf die Absperrung zu. Die Sirene heulte ohrenbetäubend, als der Krankenwagen an Nik vorüberfuhr. Über sich hörte er die knatternden Rotoren des Rettungshubschraubers. Die Straße war in bizarres Leuchten getaucht. Auf den Millionen Glassplittern spiegelte sich der Schein der Laternen, vermischt mit dem Blaulicht der Einsatzwagen. Er drängte sich durch die gaffende Menge, am Absperrband vorbei, und ignorierte die Rufe der Polizistin, die ihn aufzuhalten versuchte. Und er schrie – verzweifelt, laut, bei jedem Schritt. Alle Köpfe wandten sich ihm zu, als er an dem großen Lkw vorbeirannte, der wie ein erlegter Riese auf der Seite lag. Das Glas aus der Ladefläche lag wie ein See ausgebreitet auf dem Beton und bohrte sich mit jedem Schritt in Niks Stiefel, schnitt durch seine Sohlen bis in seine Knöchel, aber er ignorierte den Schmerz, achtete nicht auf das Blut und rannte weiter. Schreiend, mit Tränen im Gesicht.

Dann wachte er auf.

Alois war ein Psychologe aus der Klischeekiste. Er hatte schütteres blondes Haar, eine Nickelbrille und einen hageren Kör perbau. Ein Schreibblock lag auf seinem Schoß und in seiner linken Hand hielt er einen Bleistift. Immer wenn Nik etwas sagte, nickte er verständnisvoll, ohne ihn aus den Augen zu lassen. Dieser Kontakt brach nur ab, wenn Alois zu sprechen anfing. Dann hob er seine Hand ans Kinn und sah nach oben, als führe er ein Zwiegespräch mit einer höheren Macht, nicht mit seinem Patienten.

»Hat Sie der Anblick des Mannes gestern Nacht beunruhigt?« Er sprach langsam.

»Ja«, erwiderte Nik. »Es macht keinen guten Eindruck, wenn Sie in eine Klinik kommen und in der ersten Nacht verstirbt ein Patient.«

»Verstorben ist er nicht.« Alois wedelte ungeduldig mit der Hand. »Er hatte nur einen Schwächeanfall. Nicht, dass da falsche Gerüchte aufkommen.«

»Kein Mensch bekommt nachts einfach einen Schwächeanfall«, erwiderte Nik.

»Ich verspreche Ihnen, dass wir alles für das körperliche und geistige Wohl unserer Patienten tun«, flüchtete sich Alois in eine Plattitüde. »Das werden Sie heute beim Frühstück bemerkt haben.«

»In der Tat«, erwiderte Nik. Es fiel ihm schwer, seinen Sarkasmus zu verbergen. Das Frühstück war ein Albtraum an gesundem Essen gewesen. Keine Eier, kein Speck, generell nichts Fettiges, nur Vollkornbrot, das ihm jetzt noch im Magen lag, und die komischen Açaí-Bowls, ein lila Matsch, der angeblich alle Vitamine enthielt, die man für einen erfolgreichen Start in den Tag brauchte. Dazu noch Fruchtsäfte, die an Spinatkotze eines Neugeborenen erinnerten und auch so schmeckten. Einzig der Kaffee war erträglich, aber auf zwei Tassen limitiert. »So ein Frühstück hatte ich noch nie.« In Momenten wie diesem vermisste er seine Waffe. »Als besonders positiv möchte ich die Fruchtsäfte erwähnen, die mir aufgedrängt worden sind.«

»Das sind nicht nur Fruchtsäfte«, sagte Alois. »Darin steckt unser Wissen über Ernährung und Superfood. Diese Säfte haben eine entschlackende Wirkung. Das müssten Sie gemerkt haben.«

»Wenn Sie eine halbstündige Sitzung auf dem Klo mit Dünnschiss als Erfolg werten, dann ja.«

»Sehen Sie.« Der Psychologe klatschte in die Hände. »Schon haben Sie kein Problem mehr mit den fäkalen Stauungen, von denen Ihr Bruder Balthasar uns so ausführlich berichtet hat.«

»Was würde ich ohne meinen geliebten Bruder machen«, antwortete Nik überschwänglich. Er hatte noch nie so dringend einen Schluck Bier gebraucht.

Alois richtete wieder den Blick nach oben. »Kommen wir nun zu Ihnen. Worin sehen Sie den Grund für Ihre Alkoholsucht?« Er sah auf seinen Notizblock. »Ihre Leberwerte sind besorgniserregend.«

»Wie das so ist in der Jugend. Gemeinsames Trinken mit Schulfreunden, dann auf den Verbindungspartys an der Uni, später Stress im Beruf und schon ist man abhängig.«

Alois nickte verständnisvoll. »Haben Sie sich Gedanken gemacht, wie Sie sich von dieser Sucht lösen können?«

»Ich dachte, Sie flößen mir Medikamente ein, und in zwei Wochen kann ich geheilt wieder nach Hause gehen.«

Alois gab ein Schnaufen von sich, das wie das Keuchen einer asthmatischen Kuh klang. Wahrscheinlich war es ein Lachen. »Wenn es dieses Medikament geben würde, wäre sein Erfinder ein reicher Mann und die Welt um eine Krankheit ärmer.«

»Ein Freund hat mir von einer Substanz erzählt, die den Drang nach Alkohol unterdrücken soll«, erklärte Nik. »Disulfat oder so ähnlich.«

»Sie meinen Disulfiram«, erklärte Alois.

»Das war es.«

»Disulfiram ist ein schwer zu steuernder Wirkstoff, der nicht den Wunsch nach Alkohol unterdrückt, sondern zu einer Intoxikation führt. Die Folge sind gefährliche Unverträglichkeitsreaktionen, die im schlimmsten Fall zum Herzinfarkt führen können.« Er schüttelte den Kopf. »Diesen Wirkstoff bei der Behandlung von Alkoholismus einzusetzen, funktioniert nur bei höchst disziplinierten Patienten, denn ein Rückfall könnte deren Tod bedeuten.«

»Aber das ist doch die perfekte Umgebung für eine solche Behandlung«, sagte Nik. »Hier wird kein Alkohol ausgeschenkt und durch Ihre strengen Gepäckkontrollen kann er nicht eingeschmuggelt werden. Also kein Risiko, dass irgendjemand einen Herzinfarkt bekommt und ins Krankenhaus eingeliefert werden muss.«

»Eine Behandlung mit solchen Medikamenten widerspricht nicht nur unseren Grundsätzen, auch würden sich nur wenige Gäste einem solchen Risiko aussetzen wollen.«

»Glaube ich nicht.« Nik winkte ab. »Nehmen wir meinen Zimmernachbarn von heute Nacht. Ob Schwächeanfall oder Herzinfarkt, das macht keinen Unterschied.« Er zwinkerte.

Alois nahm seine Brille ab und musterte Nik genauer, als wäre er unsicher, wie er ihn einsortieren sollte.

»Geht es dem Mann gut?«, hakte Nik nach. »Ich habe gar keinen Krankenwagen wegfahren sehen.«

»Alles in Ordnung«, wiegelte Alois ab. »Wir haben den Patienten in unser Krankenzimmer gebracht und dort wieder stabilisiert.« Er machte sich eine Notiz. »Doch jetzt zu Ihnen«, wechselte er das Thema. »Wie fühlen Sie sich, wenn Sie Alkohol trinken?«

»Stark und leistungsfähig.« Nik hatte genug über Anzeichen von Alkoholkrankheit gelesen, um den Test zu bestehen. »Ich bin selbstsicherer und unbefangener in Gesellschaft anderer.« Alois nickte und machte sich eine weitere Notiz. »Bei schwieri gen Verhandlungen trinke ich vorher ein Glas, um meine Nervosität zu bekämpfen. Dann werde ich ruhiger und bin bereit, jede Herausforderung anzugehen.«

Nik gab sich Mühe, allen Klischees eines Alkoholabhängigen zu entsprechen. Doch als Alois die zweite Seite seines Notizblocks vollgekritzelt hatte, verlor er die Lust. Er wollte es auch nicht übertreiben, also erhob er sich ruckartig vom Stuhl. »Oh, meine fäkalen Stauungen.« Er griff sich an den Bauch. »Ich bin gleich wieder zurück.« Er rannte hinaus und schloss die Tür mit einem lauten Knallen, doch anstatt in sein Zimmer zu gehen, legte er sein Ohr an die Tür. Anscheinend war Alois zu seinem Telefon gegangen. Er verstand nicht viel, dazu war die Dämmung der Tür zu dick, aber es fiel der Name Nikolas Kirchhof und Disulfiram.

Nik lächelte und ging zurück in sein Zimmer. Der erste Nadelstich war gesetzt. Er war gespannt, wen er damit aufscheuchen würde.

Auch das Mittagessen war weit von dem entfernt, was Nik unter einer guten Mahlzeit verstand. Auf dem Teller lag ein Stück weißer Fisch neben gedünsteten Kartoffeln mit Möhren, Roter Bete und Kohlrabi. Natürlich ohne Soße und ohne Fett gekocht, ergänzt um eine Schüssel grünen Salat und zwei Äpfel auf einer Serviette neben dem Wasserglas. Das alleine war schlimm genug, aber das Fachpersonal der Küche wurde nicht müde, die Ausgewogenheit dieser Mahlzeit anzupreisen. Niks Nachfrage nach Salz oder Ketchup wurde mit einem Lachen quittiert, als hätte er sich einen Scherz erlaubt, gefolgt wiederum von einem Vortrag über die lebensnotwendige Vermeidung von künstlichen Fetten, chemischen Süßungsmitteln und Geschmacksverstärkern.

Nachdem er den Teller leer gegessen hatte, fühlte sich Nik hungriger als zuvor. Er nahm sich einen Apfel, nagte lust los ein Stück heraus, während er seine Mitinsassen musterte. Glücklicherweise legte die Klinik großen Wert auf Regelmäßigkeit. Es gab nur einen kurzen Zeitkorridor, in dem die Mahlzeiten ausgegeben wurden. Wer unpünktlich war, bekam nichts mehr, daher waren zu den Essenszeiten alle Patienten versammelt. Niks erste Analyse der anderen Gäste hatte nicht viel ergeben. Außer ihm waren zweiundzwanzig andere Personen in Behandlung. Siebzehn Männer und fünf Frauen. Den Jüngsten schätzte er auf gerade mal achtzehn Jahre, den Ältesten auf um die sechzig. Die meisten von ihnen waren Alkoholiker. Sie hatten die klassischen äußeren Merkmale, wie spinnennetzförmige rote Flecken auf der Haut und zittrige, schweißnasse Hände, die auf den Entzug hindeuteten. Dazu noch ein paar Kokser und Heroinabhängige. Nichts davon half, den Fall aufzuklären.

Als Nik mit seinem Apfel fertig war und zurück in sein Zimmer gehen wollte, wurde er Zeuge eines überschwänglichen Wiedersehens zweier Freunde.

»Leo«, rief ein übergewichtiger Mann, der in das Restaurant gelaufen kam. Er schwitzte stark und sein Gesicht war gerötet. Er keuchte bei jedem Atemzug und wischte sich mit dem Handrücken über die Stirn. An einem Tisch neben Nik stand ein Mann um die vierzig auf. Er hatte lockige blonde Haare und war gebräunt, als wäre er gerade von einem Urlaub aus der Karibik gekommen. Beim Lächeln zeigte er seine strahlend weißen Zähne. Passend dazu hatte er auffallend blaue Augen und kantige Gesichtszüge, die von seinem Dreitagebart etwas gemildert wurden. Besagter Leo passte eher an einen australischen Strand als in einen bayrischen Wald zur Winterzeit.

Ungeachtet des Schweißes umarmte Leo seinen Bekannten, als wären sie alte Freunde. Nichts an dieser Begegnung wäre verdächtig gewesen, hätte der blonde Mann nicht kurz seine Hand in die Hosentasche des anderen gesteckt. Die Bewegung war schnell und unauffällig, als wäre sie Leo vertraut. Sein Blick ging zu den Angestellten, die mit keiner Regung zeigten, dass sie etwas davon mitbekommen hatten. Nik beschäftigte sich mit seinem zweiten Apfel und beobachtete die beiden Männer aus den Augenwinkeln. Es gab nicht viel, was man in seiner Handfläche verschwinden lassen konnte. Kein Handy, keinen Alkohol und kaum zuckerhaltige Süßigkeiten. Geld spielte bei diesem Publikum keine Rolle, und einen USB-Stick mit geheimen Daten konnte er auch ausschließen, blieb also nur ein Päckchen mit Drogen.

Leo und sein Freund tauschten noch Nettigkeiten aus, dann setzte sich der dicke Mann an den Tisch und ließ sich sein Essen bringen, während Leo das Restaurant verließ. Nik folgte ihm und holte ihn auf dem Gang ein.

»Entschuldigung!«, rief er. Leo blieb stehen. Ein unerfahrener Straftäter würde in solchen Momenten die Nerven verlieren, hätte Nik ihn doch mit der Tat konfrontieren können, aber als sich Leo umdrehte, trug er die Maskerade eines Verkäufers, offen, freundlich und von seinem Produkt überzeugt.

»Ja, bitte«, sagte er mit einem Lächeln.

»Verzeihen Sie mir meine Aufdringlichkeit«, begann er. »Mein Name ist Nik. Ich bin neu hier und ich sterbe vor Langeweile. Können Sie mir vielleicht sagen, was man hier unternehmen kann, das nichts mit Gesprächen über die eigene Jugend, langweiligem Essen und Wellness zu tun hat?«

Einem ungeschulten Auge wäre die Veränderung an Leos Lächeln nicht aufgefallen, aber Nik hatte den Mann zum Nachdenken gebracht. Er sah ihm in die Augen, wie eine innere Musterung, überlegend, was er darauf antworten sollte.

»An was haben Sie gedacht?«

Nach Drogen zu fragen, wäre zu auffällig gewesen. Dann hätte Leo sofort gewusst, dass Nik die Übergabe bemerkt hatte. »Ein wenig Zerstreuung mit einer jungen Frau.« Nik rückte näher an Leo heran und sah sich im Gang um, als wollte er ver hindern, dass jemand mithörte. »Die Mitpatientinnen sind mir zu alt.«

Leo ließ seinen Blick über Nik streifen, als wäre er noch immer unsicher, wie er ihn einzuschätzen hatte. »Dann ist heute Ihr Glückstag«, sagte er schließlich. »Jeden zweiten Mittwoch veranstalten wir eine zwanglose kleine Party im Yogaraum.«

»Eine Party?«, wunderte sich Nik. »Ich dachte, hier sind alle Arten von Drogen verboten.«

»Laute Musik ist keine Droge«, erklärte Leo. »Und da hier keine Sexsüchtigen behandelt werden, drücken die Angestellten ein Auge zu und erlauben ausgewählten … Damen den Besuch.« Er zuckte die Achseln. »Dafür gibt es nur Fruchtcocktails, Milchshakes und leicht bekömmliches Fingerfood.«

»Das hört sich nach der perfekten Zerstreuung an.«

»Um 20.00 Uhr geht es los. Bis dann.« Leo hob einen Daumen und ging weiter. Nik wartete, bis sein neuer Freund außer Sicht war, und ging dann zu seinem Zimmer zurück. Jetzt wusste er, wie Olga in diese Klinik gelangt war.

Er war gespannt, was er heute Abend erleben würde.

Nik war noch nicht im Yogaraum gewesen, aber die laute Musik hatte ihm den Weg gewiesen. Der dumpfe Bass dröhnte und brachte die Fensterscheibe zum Vibrieren. In einer Ecke des Raums stand ein DJ mit Kopfhörern auf den Ohren, im Takt nickend, während er Knöpfe an einem Mischpult drehte. Daneben war ein Tisch mit spanischen Tapas, griechischen Oliven und handlichen Vollkornschnitten aufgebaut. Hinter einer kleinen Theke zerteilte ein Barkeeper Früchte, aus denen er rote Drinks mischte, die er auf einem kleinen Stelltisch anrichtete. Ihm ging eine junge Frau zur Hand, die Gläser einsammelte, abspülte und leere Flaschen wieder auffüllte.

In der Mitte standen die männlichen Patienten und unterhielten sich mit jungen Frauen, deren knapp sitzende Kleidung und hohe Schuhe keinen Zweifel daran ließen, warum sie hier waren. Etwas weiter war ein gut aussehender Südländer in ein Gespräch mit drei Patientinnen vertieft. Er wirkte wie ein spanischer Torero, groß, kräftig, mit langem, lockigem Haar, über dessen rasierter Brust sich ein durchsichtiges Hemd spannte.

Nik zählte sechzehn Patienten, aber keine Angestellten. Kein Alois, der das Verhalten beobachtete, kein Gunnar, der für Ordnung sorgte, und auch keine Pia.

»Nik«, rief jemand seinen Namen über die laute Musik. Leo kam auf ihn zu und umarmte ihn. »Es stört dich doch nicht, dass ich dich duze.« Der blonde Mann war wie ausgewechselt. Sein Misstrauen war verschwunden, als er ihm die Hand schüttelte. »Mein Name ist Leopold von Waldbach, aber meine Freunde dürfen mich Leo nennen.«

»Freut mich, dich kennenzulernen.«

Leo reichte ihm einen Drink aus einer rötlichen Masse. »Schmeckt wie Erdbeer-Margarita, nur ohne Tequila.«

Nik prostete ihm zu und trank einen Schluck. Die Erdbeeren waren für diese Jahreszeit erstaunlich frisch und der darin verarbeitete Sirup erzeugte eine angenehme Süße, aber ohne den Tequila fehlte der Kick.

Leo deutete auf eine junge Frau. Sie war kaum volljährig, hatte auffällig rot gefärbte Haare und überragte mit ihren hochhackigen Stiefeln die meisten der Anwesenden. Ihre Lippen waren von einem dunklen Blau und ihr Gesicht war blass geschminkt. Ein schwarzer Lederrock betonte ihren prallen Hintern.

»Allererste Ware«, sagte Leo, als er Niks Blick bemerkte.

»Wie habt ihr die Mädchen in die Klinik geschmuggelt?«

»Es gibt eine Absprache mit der Geschäftsleitung«, erklärte Leo. »Früher sind die Patienten aus lauter Verzweiflung übereinander hergefallen, was sich schlecht auf die Stimmung in der Klinik und auf die Behandlungserfolge ausgewirkt hat.« Er nahm einen Schluck von seinem Drink. »Und jetzt lassen sie alle zwei Wochen Prostituierte herein, um den schlimmsten Drang zu mildern.« Er deutete auf den Mann mit den schwarzen Locken. »Auch unsere Damen sind ganz verzückt.«

»Und wie läuft das Schäferstündchen ab?«, fragte Nik. »Muss ich eine Nummer ziehen?«

»Das geht nach Aufenthaltsdauer«, erklärte Leo. »Mein dicker Freund dort drüben ist Waldemar, ein Dauergast in dieser Einrichtung. Beim letzten Mal hat man ihn mit drei Komma eins Promille in einem Champagnerbad wiederbelebt. Das war vor vier Monaten.«

»Eine lange Zeit für einen Klinikaufenthalt.«

»Könnte man meinen, aber Waldemar steht erst auf Platz sechs.« Er wandte sich wieder Nik zu. »Was ist dein Problem?«

»Zu viele Partys mit zu viel Alkohol und zu vielen Ausfällen«, begann er. »Zumindest sagt das mein Vater. Daher hat er mich vor die Wahl gestellt, entweder Entzug in der Klinik oder er dreht mir den Geldhahn zu und spendet mein Erbe einem Trachtenverein.« Nik trank einen Schluck von dem Cocktail. »Und bei dir?«

»Wiederholtes Fahren ohne Führerschein unter Drogeneinfluss«, sagte Leo. »Selbst meine gut bezahlten Anwälte hätten mir dieses Mal den Knast nicht ersparen können, weil ich mit hundertzwanzig einen Schulbus gerammt habe.«

»Hundertzwanzig geht noch.«

»Nicht in einer verkehrsberuhigten Zone«, ergänzte Leo.

»Oh«, sagte Nik.

»Ich bin seit vier Wochen hier und warte, bis die Ärzte mich als geheilt entlassen.«

»Kann man da nichts machen?«, fragte Nik. »Vielleicht durch eine großzügige Spende für den Bau eines weiteren Yogaraums?«

»Habe ich schon versucht«, sagte Leo. »Das und andere Tricks, aber man kommt hier nur wieder raus, wenn man mitspielt.« Er nahm Nik das Glas ab. »Die Erdbeersmoothies sind langweilig«, wechselte er das Thema. »Der Barkeeper hat uns etwas Neues mitgebracht.« Er ging zur Theke, redete mit dem jungen Mann und bekam zwei Gläser gereicht. Eines davon gab er Nik. »Champine«, erklärte Leo. »Ein alkoholfreier Champagner mit Kohlensäure auf Basis von Kiefernnadeln.« Er prostete Nik zu und leerte das Glas in einem Zug.

Nik folgte Leos Beispiel. Der Geschmack war abstoßend. Er musste sich zusammenreißen, die Flüssigkeit nicht sofort auszuspucken. Es schmeckte wie ausgepresste Baumrinde.

»Und wie ist es?«

»Fürchterlich.«

Leo schlug ihm lachend auf die Schulter. »Kein Problem. Ich habe noch zwei Stunden, bis mich die Rothaarige auf mein Zimmer begleitet. Bis dahin finden wir etwas, das dir schmeckt.«

Als Nächstes musste Nik ein Getränk namens Seedlip probieren, ein klares Wasser, das mit Gewürzen und Kräutern angereichert worden war. Bei der folgenden Mischung aus Gemüsesaft und Waldmeisteressig spürte er, wie seine Knie zittrig wurden.

»Alles in Ordnung?«, fragte Leo.

»Und da ist kein Alkohol drin?«, wollte Nik wissen. Der Raum drehte sich um ihn. Die Musik wurde dumpfer, als wären nur noch die Bässe eingeschaltet. Er versuchte sich an der Wand abzustützen.

»Kann mir mal jemand helfen?«, hörte er Leos Stimme noch. »Unser Neuer ist noch nicht in der Spur.« Dann sank er zu Boden und verlor das Bewusstsein.

Nik erwachte in seinem Bett, die Kleidung vom Abend noch angezogen. Sein Kopf schmerzte, er hatte einen abstoßenden Geschmack nach Kiefernnadeln im Mund und seine Augen waren verklebt. Sein Wecker zeigte 9.23 Uhr. Die Frühstückszeit war vorbei, aber schon der Gedanke an Essen ließ ihn würgen. Er wankte ins Bad, öffnete den Hahn und ließ sich kaltes Wasser über den Kopf laufen, bis er wieder klar denken konnte. Müde nahm er sich ein Handtuch und trocknete seine Haare, während er sich im Spiegel musterte. Trotz adretten Haarschnitts und Rasur sah er fürchterlich aus. Seine Augen waren rot und geschwollen. Speichel klebte am Ärmel seines verknitterten Hemdes und der oberste Knopf war abgerissen. Nik warf das Handtuch in die Dusche, ging zu seinem Kleiderschrank und zog seinen metallenen Koffer hervor. Neben den Rollen befand sich ein schmales Fach, das er mit einer dünnen Nagelfeile öffnete. Er zog ein kleines Klapphandy heraus. Da das Röntgengerät der Klinik nicht so hochwertig war wie jene am Flughafen, hatte eine Bleiverkleidung genügt, um die Mitarbeiter täuschen zu können. Als er das Telefon einschaltete, war er über den guten Empfang überrascht. Nicht weit von der Klinik musste ein Funkmast sein. Er wählte Jons gespeicherte Nummer.

»Guten Morgen«, meldete dieser sich nach dem zweiten Klingeln.

»Ein guter Morgen ist es nicht«, brummte Nik. Seine Stimme war rau, wie nach einem Besäufnis.

»Wärst du nicht in einer Alkoholentzugsklinik, würde ich vermuten, dass du heute Nacht versackt bist.«

»Genau das ist passiert, aber ich habe den Ausfall nicht wegen zu viel Alkohols, sondern weil mir jemand etwas in den Drink getan hat.«

»Hast du die falschen Fragen gestellt?«, wunderte sich Jon.

»So weit bin ich nicht gekommen, aber wer immer das gemacht hat, hat mein Zimmer durchsucht, während ich geschlafen habe.«

»Und das hast du trotz Betäubung mitbekommen?«

»Nein«, brummte Nik. »Der Haartrick.«

»Ich habe keine Ahnung, wovon du redest.«

»Du solltest mehr alte James-Bond-Filme sehen.« Nik nahm eine Flasche Wasser aus dem Kühlschrank und trank einen Schluck. »Ich klebe an jeden Schrank ein Haar zwischen Tür und Korpus. Unten angebracht ist es unmöglich zu sehen, und wenn die Tür geöffnet wird, fällt es zu Boden.«

»Und wie viele Haare liegen am Boden?«

»Alle«, sagte Nik und leerte die Flasche. »Aber dass man mich betäubt und das Zimmer durchsucht hat, ist ein gutes Zeichen. Man fürchtet sich vor neugierigen Patienten, also gibt es etwas zu verbergen.«

»Wer hat dich betäubt?«

»Ein neuer Freund«, erklärte Nik, »aber ich weiß nicht, ob das etwas mit unserem Fall zu tun hat oder ob er einfach Angst hat, dass ich ihm sein Geschäft verderbe.« Er trank wieder einen Schluck Wasser. »Hast du einen Bezug von Viola zu dieser Klinik gefunden?«

»Nein«, erwiderte Jon. »Ich weiß, wie Olga in die Einrichtung gekommen ist, aber ich habe keine Ahnung, wie Viola hierzu passt. Vielleicht gibt es eine Akte von ihr.«

»Das glaube ich nicht, aber ich werde das Tempo erhöhen und bei den richtigen Gelegenheiten ihren Namen fallen lassen. Vielleicht springt jemand darauf an. Bis dahin kümmere ich mich um Kathrins Patientenakte.«

»Ich habe die ganze Nacht versucht, Zugang zu Klinikdaten zu erhalten, aber nichts gefunden – als wäre die Klinik nicht im Netz.«

»Ist sie auch nicht«, bestätigte Nik. »Mein Betreuer hat eine altmodische Akte von mir und schreibt seine Einträge mit der Hand. In seinem Zimmer stehen zwei abschließbare Schränke mit Platz für Hunderte Fälle. Der Computer in seinem Raum ist entweder nur Fake oder für andere Dinge.«

»Rückständig«, murmelte Jon.

»Dafür sicher«, sagte Nik. »Es gibt keine Möglichkeit, die Daten herunterzuziehen, weder für Hacker wie dich noch für investigative Journalisten.«

»Wie willst du dann an Kathrins Akte kommen?«

»Auf die altmodische Art. Durch Schlösserknacken.«

»Deine Dietriche habe ich dir in das Geheimfach mit eingebaut«, erklärte Jon.

»Nützt mir bei den elektronischen Türen nichts. Die Mitarbeiter benutzen personalisierte Karten zum Öffnen der Schlösser. Da brauche ich meine flinken Finger.«

»Du hast Fingerfertigkeit?«, wunderte sich Jon.

»Du wärst überrascht«, sagte Nik. »Ich habe über hundert Einbrüche und Diebstähle in meiner Laufbahn beim KDD bearbeitet. Da lernt man einiges.« Er setzte sich müde auf das Bett. »Wenn alle schlafen, werde ich mir den Aktenschrank vornehmen. Ich bin gespannt, welche Geheimnisse dann zutage treten.«





