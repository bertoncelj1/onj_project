
Das sonnige Wetter passte zu Niks Stimmung. Der Schnee war auf dem Rückzug, während er mit der Tageszeitung in der Hand durch den Englischen Garten spazierte.

»Ich habe dich schon lange nicht mehr so zufrieden erlebt«, sagte Mira. Trotz der wärmeren Temperaturen hatte sie eine dicke Winterjacke an, einen Schal um den Hals gebunden und eine Mütze auf.

Nik deutete auf die Titelseite. Unter dem Bild der Klinik stand in großen Lettern: »Medikamentenskandal aufgedeckt!« Darunter war ein Foto von Dr. Gawinski.

Sie nickte anerkennend. »Wie habt ihr das geschafft?«

»Mit einer Mischung aus Beweisen und Fälschungen«, begann Nik. »Die Unterlagen, mein Blut und die Proben, die ich mit Leo geklaut habe, konnten zweifelsfrei beweisen, dass Gawinski an seinen Patienten Versuche mit einer auf Disulfiram basierenden Substanz gemacht hat, ohne deren Wissen.«

»Zu welchem Zweck?«

»Zur Bekämpfung der Alkoholsucht. Die Verbindung von Disulfiram und Alkohol kann tödlich sein, aber wenn man die Nebenwirkungen auf Unwohlsein und maximal Erbrechen her unterfahren könnte, hätte man einen guten Therapieansatz und könnte Milliarden scheffeln.«

»Klingt eigentlich nach einer guten Sache.«

»Wenn Gawinski nicht die Gesundheit seiner unbeteiligten Patienten dabei riskiert hätte, denn nach Balthasars Auswertungen hat er nur minimale Fortschritte gemacht.«

»Warum hat er nicht legal daran geforscht und Tests an Freiwilligen gemacht?«

»Weil er keine Zulassung bekommen hat. Daher hat er seine Patienten als Versuchskaninchen benutzt. Das Geld, das er mit ihnen verdiente, hat er in die Forschung umgeleitet, um seine Methode weiter zu verfeinern.«

»Gab es Tote?«

»Balthasar vermutet in zwei Fällen, dass die Wechselwirkung der Medikamente mit Alkohol zum Tode geführt hat, aber das ist jetzt Aufgabe der Staatsanwaltschaft. Weiterhin haben wir der Presse den Teil der Unterlagen zugesandt, aus denen sich reißerische Schlagzeilen machen lassen. Damit stellen wir sicher, dass Gawinskis Ruf ruiniert ist.«

»Und was habt ihr gefälscht?«

»Den Bericht über Leos Tod«, sagte Nik. Es schmerzte ihn noch, wenn er an ihn dachte. Wie er auf die Sicherheitsleute zuwankte, im gespielten Alkoholrausch, nur damit Nik entkommen konnte. »Wir haben ein Schreiben von Gawinski an Alois und den Chef der Sicherheit in der Klinik gefälscht. In diesem schlägt er vor, Leopold von Waldbach zum Schweigen zu bringen, weil dieser hinter das Geheimnis der Medikamentenversuche gekommen ist. Da ich die Daten vor Leos Tod aus dem Computer gezogen habe, war zu dem Zeitpunkt noch nichts darüber vermerkt. Damit schieben wir die Ermittler in die richtige Richtung.«

»Was ist wirklich mit ihm passiert?«

»Auf dem Totenschein ist von einem tödlichen Sturz die Rede, aber der Staatsanwalt hat eine Obduktion angeordnet. Dieses Thema steht wegen des Presseleaks so in der Öffentlichkeit, dass bei Leo keine Macht der Welt etwas vertuschen kann. Anders als bei Kathrins Tod.«

»Und was ist mit ihr, Viola und Olga?«

»Jon hat Einträge zu Kathrins Aufenthalt gefunden, aber nichts zu Viola oder Olga. Wer immer für das Verschwinden der Mädchen verantwortlich ist, es hatte nichts mit den Medikamententests zu tun.«

»Dann war es Zufall, dass alle drei in der Klinik waren?«

»Niemals«, sagte Nik bestimmt. »Es ist die einzige Verbindung, die wir haben. Auch wenn wir keinen Zusammenhang zu den Versuchen finden konnten, gibt es einen zu den Mitarbeitern oder zu den Patienten. Wir müssen nur herausfinden, zu wem.«

»Wie wollt ihr das anstellen? Du bist suspendiert, also kannst du weder Gawinski noch einen Patienten befragen.«

»Mit einer Zeitleiste«, erklärte Nik. »Wir wissen, wann Kathrin in der Klinik war. Weiterhin kennen wir den Zeitpunkt der Partys, bei denen Olga und Viola gearbeitet haben könnten. Dann nehmen wir alle Namen der Mitarbeiter und Patienten, die gleichzeitig mit Kathrin in der Klinik waren, und gleichen deren Fotos mit den Besuchern des Nachtclubs ab, und zwar von jenem Abend, an dem Viola verschwunden ist.«

»Klingt nach sehr viel Arbeit.«

»Oh, ja.« Nik lächelte. »Aber nur für denjenigen, der sich mit Computern auskennt.«

»Du scheinst Gefallen an diesem Jon gefunden zu haben.«

Nik strafte Mira mit einem abfälligen Seitenblick. »Er hat mich bedroht, erpresst und gegen meinen Willen in den Fall gedrückt. Ich wäre beinahe erschossen worden, außerdem wurde ich zweimal niedergeschlagen und gefoltert. Auf Umwegen ist er auch an meiner Suspendierung schuld.«

»Aber er hat einen Medikamentenskandal aufgedeckt …«

»Zufall«, unterbrach Nik.

»… und hilft vielleicht dabei, einen Serientäter zu schnappen, der sonst unbehelligt geblieben wäre, von den Verstrickungen in die Kripo ganz zu schweigen.«

Nik brummte mürrisch.

»Außerdem hat er dir den Spaß an deinem Beruf zurückgegeben, denn so zufrieden und redselig warst du lange nicht mehr.«

»Jetzt übertreibst du.«

Mira lachte. »Jons Methoden, dich in den Fall zu drängen, mögen nicht anständig gewesen sein, aber er hatte einen guten Instinkt, dass er dich dafür ausgewählt hat.«

Das Klingeln seines Handys unterbrach Niks Antwort. »Wenn man vom Teufel spricht«, murmelte er und nahm den Anruf entgegen. »Was gibt’s, Jon?«

»Ich bin fertig«, seufzte dieser. »Das war eine Höllenarbeit, aber wir haben zwei neue Verdächtige. Beide waren zu Kathrins Zeit in der Klinik und beide habe ich auf den Aufnahmen der Disco gefunden, auch wenn der Bildabgleich nicht perfekt ist.«

»Hört sich gut an.«

»Spring in deinen Mietwagen und mach dich auf den Weg nach Obergiesing«, sagte Jon. »Die Details zum ersten Verdächtigen gebe ich dir auf der Fahrt.«

Die Wohnung hätte ein Traum sein können. Zweihundert Quadratmeter Loft. Der Boden mit dunklem Parkett ausgelegt, darauf hellgraue Teppiche, eine weiße Couch mit Blick auf einen Garten voller Apfelbäume. Eine moderne Küche, ein großer LED-Fernseher an der Wand und ein Jacuzzi auf der Terrasse. Doch der Besitzer hatte alles verkommen lassen. Auf dem Boden lagen ungeöffnete Briefe, Zeitungen und leere Champagnerflaschen zwischen Fast-Food-Kartons und benutzter Klei dung. Auf der weißen Couch waren große Kaffeeflecken und auf dem Herd stapelten sich benutzte Teller. Der Vorhang zur Terrasse war heruntergerissen und ein Stuhl mit abgebrochenem Bein lag daneben.

»So eine Unordnung kenne ich nur von Junkies«, sprach Nik in das Telefon, während er durch das seitliche Wohnzimmerfenster blickte. »Nur haben die nicht das Geld, sich eine solche Behausung zu leisten.«

»Silvio Verbeck ist der Spross eines Immobilienmoguls, dem Häuser um den Marienplatz und in der Kaufingerstraße gehören. Eines davon hat ihm sein Vater vererbt, was Silvio ein regelmäßiges Einkommen beschert.«

»Was arbeitet er?«

»Er hat Beteiligungen an einer Bar und zwei Restaurants, bei denen er auf jedem zweiten Bild zu sehen ist, wenn man die sozialen Netzwerke durchforscht.«

»Sonst etwas?«

»Sechsunddreißig Jahre alt. Gebürtiger Münchner. Wurde zweimal wegen Drogenvergehen festgenommen. Hat deshalb keinen Führerschein mehr, ist aber nie ins Gefängnis gegangen.«

»Irgendwelche Gewaltdelikte?«

»Nein«, antwortete Jon. »Er war zweimal Gast in Gawinskis Klinik, bei seinem ersten Besuch sechs Wochen, beim zweiten Mal drei Monate. In der Zeit muss er unseren Frauen begegnet sein.«

»Ich klopfe mal an.« Nik ging vom Fenster weg zum Eingangsbereich. Die Tür war mit einem kleinen Vordach versehen. Nik bemerkte zwei Kameras, eine direkt bei der Klingel, die andere am Rand des Vordachs. Der Boden war mit hellem Gestein ausgelegt und der kleine Vorgarten war gepflegt. Zumindest äußerlich wollte Silvio Verbeck den Schein wahren.

Nik drückte die Klingel. Der laute Gong war bis nach draußen zu hören. Eine Zeit lang passierte nichts, sodass Nik bereits überlegte, ob er in die Wohnung einbrechen sollte, als die Gegensprechanlage knisterte.

»Hallo?« Die Stimme klang verschlafen.

»Guten Tag, Herr Verbeck. Kommissar Pohl von der Kripo München.« Nik hielt seine falsche Kripomarke in die Kamera. »Ich muss mit Ihnen über Ihren Besuch in der Beautyfarm an den Wiesen reden.«

»Kenne ich nicht«, kam die Antwort.

»Vielleicht haben Sie es nicht gelesen, aber die Klinik wurde geschlossen und alle Unterlagen wurden von der Staatsanwaltschaft beschlagnahmt«, log Nik. »Darunter auch Ihre Akte.«

»Das sind vertrauliche Informationen«, fuhr Verbeck auf. »Außerdem habe ich jetzt keine Zeit. Rufen Sie meinen Anwalt an.«

»Herr Verbeck«, sagte Nik ruhig. »Ich bitte Sie nur um ein kurzes Gespräch. Ich stelle Ihnen ein paar Fragen und dann bin ich wieder weg. Alternativ kann ich meine Kollegen vom Drogendezernat unter einem Vorwand einbestellen und Ihre Wohnung durchsuchen lassen. So oder so werden Sie mit mir reden.«

Verbeck schwieg. Dann ertönte das Surren des Türschlosses und Nik trat ein.

Der Gestank war schlimmer als die Unordnung. Das Wohnzimmer war seit Tagen nicht mehr gelüftet worden. Darüber der beißende Geruch getrockneten Alkohols und gammelnden Essens. Verbeck erfüllte alle Klischees eines wohlhabenden Drogenabhängigen. Seine braunen Haare standen wild von seinem Kopf ab. Er war unrasiert und seine Gesichtshaut war von einer kränklichen Bleiche. Er hatte eine kleine Wunde neben dem Auge, die von einer Schlägerei stammen mochte. Sein weißes Maßhemd war zerknittert und fleckig. Zwei Knöpfe waren offen, ebenso wie der Reißverschluss seiner Lagerfeld-Jeans. Er trug eine Bugatti-Sonnenbrille und einen protzigen Goldring am rechten Zeigefinger. Seine Hände zitterten und Schweiß stand ihm auf der Stirn. Er nuschelte nur ein kurzes »Hallo«, als er Nik die Hand schüttelte. Verbeck roch ungewaschen, und der Geruch, der dem Mund entwich, war nicht angenehmer, trotz gebleachter Zähne. Nik deutete auf die Couch und beide setzten sich hin.

»Herr Verbeck, warum waren Sie Gast in der Beautyfarm an den Wiesen?«

»Sicher nicht wegen der Maniküre«, erwiderte er schnippisch und fuhr mit dem Handrücken über seine Nase. Verbeck zeigte alle Zeichen eines Junkies auf Entzug, was vorteilhaft war. Er würde alles tun, um Nik schnellstmöglich wieder loszuwerden.

»Welches … Problem haben Sie, wenn ich fragen darf?«

»Drogen.«

»Welcher Art?«

»Koks, verdammt«, fuhr Verbeck Nik an. »Ich ziehe mir ab und zu was durch die Nase. Na und? Das Zeug hat Coca-Cola früher in seine Getränke gemischt. Damals hat es noch niemand gestört. Sollte man wieder einführen. Würde so manchen lockerer machen.« Er presste seine Hände in den Schoß in dem Versuch, das Zittern zu verbergen.

Nik nahm seinen Block zur Hand und machte sich eine Notiz, um das Gespräch weiter in die Länge zu ziehen. Verbecks Blick huschte hin und her wie der eines Irren auf der Suche nach seinen eingebildeten Verfolgern. Dabei blieben seine Augen oft an einem antiken Sekretär hängen. Wahrscheinlich bewahrte er dort seine Drogen auf.

»Haben Sie in der Klinik Bekanntschaften gemacht?«

»Bekanntschaften?«

»Andere Patienten, Mitarbeiter, Ärzte …«, führte Nik aus. »Mit wem hatten Sie den Tag über zu tun?«

»Meist mit dem Drecksack Alois«, empörte er sich wieder. »Schon bei unserem ersten Gespräch hatte ich das Bedürf nis, ihm eine reinzuhauen. Wie er immer den Blick zur Decke gerichtet hat, wenn er über etwas nachdachte, und seine ganze gekünstelte Art.«

»Sonst noch jemand?«

»Na klar.« Er gestikulierte wild. »Kursleiter, Putzfrau, Leute im Restaurant … Was soll die Frage?«

»Auch einen gewissen Leopold von Waldbach?«

»Nie gehört.«

»Peter Maier«, erfand Nik einen Namen.

»Nein.«

»Und was ist mit Viola Rohe?«

Verbecks Blick huschte zu Nik. Er zögerte nur einen Augenblick, aber das war Antwort genug. »Ich kenne keine Viola.«

»Das ist seltsam«, erwiderte Nik. »Viola war zur gleichen Zeit wie Sie in der Klinik.«

»Da waren eine Menge Patienten. Glauben Sie, ich habe mich jedem vorgestellt?«

»Viola war keine Patientin. Sie hat bei Ihrer Mittwochsorgie ausgeholfen, nicht als Prostituierte, sondern beim Getränkeausschank. Fällt es Ihnen jetzt wieder ein?«

Wieder die Panik im Blick. »An der Party habe ich nie teilgenommen.«

Nik hätte beinahe gelacht, so schlecht trug Verbeck die Lüge vor. »Eigenartig«, fuhr er fort. »Ich habe eine Aufnahme von Ihnen von der Nacht vom 22. auf den 23. Oktober 2016 vom Nachtclub Palastbau. Und raten Sie mal, wer dort hinter der Bar gearbeitet hat?«

»Verschwinden Sie.« Verbeck sprang von der Couch auf. »Ich muss mir das nicht länger anhören.«

Nik steckte seinen Notizblock wieder in die Tasche. »Doch«, erwiderte er. »Nur in einer anderen Umgebung.«

»Raus, raus, raus.« Verbeck wedelte aufgeregt mit den Händen.

Nik erhob sich und ging mit einem Lächeln zur Tür, die sich mit einem Knall hinter ihm schloss. Sein Plan war aufgegangen. Sie hatten einen neuen Verdächtigen.

Da Verbeck die Jalousien zur Straße heruntergelassen hatte, kletterte Nik über den Zaun in den Garten und beobachtete den neuen Verdächtigen verstohlen durch die vollverglaste Rückwand zur Terrasse. Eine Hecke bot ihm ausreichend Deckung.

Verbeck hatte sein Mobiltelefon in der Hand und gestikulierte wild beim Reden. Die Glasfront war gut isoliert, aber trotzdem vernahm er Wortfetzen.

»Und wieso kommt er dann in mein Haus?«, schrie Verbeck und trat an den Couchtisch. Seine Stimme war schrill. Sein Körper zitterte. Er diskutierte noch eine Minute, dann warf er das Handy auf die Couch und verließ den Raum, wohl in Richtung Badezimmer.

Behände stieg Nik wieder über den Gartenzaun, setzte sich in den Mietwagen, den Jon ihm besorgt hatte, und wählte dessen Nummer.

»Ich bin zuversichtlich«, begann er das Gespräch. »Kaum habe ich Violas Namen erwähnt, ist er hochgegangen wie eine Rakete und hat mich aus der Wohnung geworfen.«

»Also könnte er unser Mörder sein?«, fragte Jon.

»Der Mörder, vielleicht, aber in seinem Zustand kann er keine so subtile Vertuschung durchführen. Weder hätte sich Tilo mit einem solch abhängigen Junkie eingelassen, noch hätte er eine Gerichtsmedizinerin wie Dr. Cüpper dazu gebracht, ihre Karriere für ihn zu riskieren. Verbeck ist nicht das Ende der Nahrungskette.« Nik seufzte. Noch immer verstand er das große Ganze nicht. »Wie sind seine familiären Verhältnisse?«

»Verbecks Vater ist verstorben. Seine beiden älteren Brüder verwalten den größten Teil der Familienimmobilien. Seine Mutter lebt in Marbella. Wenn man den Gerüchten glaubt, sind die Brüder untereinander zerstritten. Bei einer Benefizveranstaltung der Firma wurde Silvio der Zutritt verweigert.«

»Also können wir die Verbeck-Familie als Unterstützer ausschließen.«

»Wahrscheinlich«, bemerkte Jon. »Was hast du jetzt vor?«

»Kaum war die Tür hinter mir ins Schloss gefallen, hat Verbeck jemanden angerufen, mit dem er sich gestritten hat. Irgendetwas wird heute noch passieren. Entweder bekommt Verbeck noch Besuch, oder er verlässt das Haus, um sich mit jemandem zu treffen. Bis dahin warte ich im Auto.«

»Lohnt sich ein Einbruch?«, fragte Jon.

»Wird schwierig«, sagte Nik. »Türen und Fenster sind kaum zu überwinden. In den Garten gelangt man leicht, aber die Verglasung ist aus Sicherheitsglas. Wenn er nicht die Terrassentür offen lässt, komme ich nicht hinein. Auch habe ich nichts Interessantes entdeckt. Kein Computer, nur ein Tablet. Keine Ordner oder Aufzeichnungen, nicht einmal Bücher. Einzig sein Handy könnte uns weiterbringen, aber dafür riskiere ich den Einbruch nicht. Das kann ich Verbeck unterwegs abnehmen. Wenn er wieder auf Koks ist, bemerkt er nicht einmal, wenn ihn ein rosa Elefant verfolgt.«

»Solange du auf Verbeck wartest, versuche ich mehr über unseren zweiten Verdächtigen herauszufinden.«

»Wer ist das?«

»Ein junger Geschäftsmann namens Eberhard Lossau. Ich weiß noch nicht viel, aber er ist auf Geschäftsreise in China und kommt erst in ein paar Tagen zurück, daher können wir uns auf unseren Junkie konzentrieren.«

Die Tür des Wohnhauses ging auf und Verbeck kam heraus. Er trug eine dicke Winterjacke, und eine Wollmütze verbarg seine Haare. Er hielt den Blick zu Boden gesenkt, wie ein Hollywoodstar, der Angst hat, erkannt zu werden.

»Showtime«, sagte Nik und verließ das Auto. »Ich bin gespannt, ob mich mein neuer Freund zum großen Strippenzieher führen wird.«

Glücklicherweise ging Verbeck zu Fuß. Eine unauffällige Verfolgung im Münchner Stadtverkehr wäre eine Herausforderung gewesen. An diesem Mittwochmittag war nicht viel los auf dem Gehweg. Das Wetter hatte sich gebessert, aber es war immer noch kalt genug, dass niemand auf die Straße ging, wenn er nicht musste. Verbeck wartete an einer Ampel, als sein Handy klingelte. Er nahm einen weißen Bluetooth-Kopfhörer aus der Tasche, steckte sich diesen in die Ohren und begann zu sprechen. Seine Stimme war ruhig. Er gestikulierte nicht mehr und schrie nicht. Weil er sich auf das Telefonat konzentrierte, war es leichter, ihm zu folgen. Andererseits hätte Nik gern gewusst, über was sein Ziel sprach, er musste aber den großen Abstand beibehalten. Es gab auf seiner Straßenseite kaum Geschäfte, in die er ausweichen konnte, sollte sich Verbeck umdrehen. Auch waren zu wenige Menschen unterwegs, um in einer Gruppe untertauchen zu können.

Zu Niks Glück drehte sich Verbeck kein einziges Mal um. Fünfzehn Minuten später blieb er auf einer Kreuzung stehen und sah sich um, als suche er den richtigen Weg. Er drückte die Hand auf das Ohr, als erhalte er Anweisungen. Schließlich nickte er und ging in eine schmale Seitengasse hinein. Nik blieb an der Ecke stehen und beobachtete Verbeck, der immer wieder nach links und rechts sah, als suche er die richtige Hausnummer. An einem Gebäude mit einem Container davor blieb er schließlich stehen. Er nickte wieder. Dann betrat er das Haus.

Nik wartete, bis Verbeck verschwunden war, und folgte ihm dann hinein. Das Haus war ein klassischer Münchner Altbau, aus braunem Sandstein, mit aufwendig verzierten Fenstergiebeln, einem kleinen Vorgarten und einem hohen Metallzaun. Die Türen zum Haus waren angelehnt, als hätte man auf Ver beck und ihn gewartet. Nik ging vorsichtig hinein und sah nach oben. Es roch nach Estrich, das Geländer der breiten Holztreppe war mit Plastikfolie abgehängt. Ebenso waren der Boden und die Stufen abgedeckt. Wahrscheinlich wurde das Innere grundsaniert. Nik schloss die Augen und verließ sich auf sein Gehör. Er vernahm weder Schritte noch Geräusche, die auf die Anwesenheit von Handwerkern schließen ließen. Vor der Tür standen keine einschlägigen Fahrzeuge, also schien heute niemand zu arbeiten. Von oben hörte er Verbeck leise sprechen. Obwohl er keine Worte verstehen konnte, wartete Nik, ob sich noch eine andere Stimme in das Gespräch einmischte. Er vernahm niemanden sonst, also war sein Verdächtiger alleine.

Die ganze Sache gefiel Nik nicht. Normalerweise hätte er das Gebäude umrundet und inspiziert, aber wenn Verbeck ihn bemerkte, würde er wieder verschwinden. Nik vermisste seine Dienstwaffe. Er hoffte nur, dass oben keine Überraschung auf ihn wartete. Zu seinem Leidwesen knarzte die Treppe bei jedem Schritt, aber Verbeck schien die Störung nicht zu bemerken und telefonierte unvermindert weiter. Seine Stimme war ruhig, fast monoton, im Gegensatz zu seinem Gebaren bei sich zu Hause. Im ersten Stock ging Nik in eine Wohnung. Die Tür war ausgehängt, der Bodenbelag herausgeschlagen. Die Lampen an der Decke waren abgeklebt. Mit jedem Schritt in Richtung Außenwand wurde Verbecks Stimme lauter. Nik schlug eine Plane zur Seite und kam in ein großes Zimmer mit Erker. Der Verdächtige stand vor dem Fenster. Er hatte den Kopfhörer abgenommen, das Handy in der Tasche und hörte zu sprechen auf, als Nik eintrat. Verbeck wirkte nicht überrascht, Nik zu sehen. Er machte ein trauriges, bedauerndes Gesicht.

»Es tut mir leid«, sagte er und senkte den Kopf.

Bevor Nik zu einer Frage ansetzen konnte, hörte er das Klicken eines Pistolenhahns. Durch eine Seitentür trat eine schlanke Frau, in der Hand eine SIG P226, die sie auf Niks Herz gerichtet hielt. Eine effiziente Waffe. Aufgrund des sehr kurzen Stellwegs hätte Nik drei Kugeln in der Brust gehabt, bevor er sich nur am Kopf gekratzt hätte.

»Guten Tag, Herr Pohl. Ich freue mich, Sie endlich kennenzulernen.« Sie hatte einen leichten norddeutschen Einschlag bei der Betonung der Worte.

Nik hob die Hände und betrachtete die Frau genauer. Sie war in einen dunklen Hosenanzug gekleidet. Der Schnitt war modern und die gute Passform ließ auf Maßanfertigung schließen. Ihre blonden Haare waren am Hinterkopf zusammengebunden. Abgesehen von ihrer etwas zu groß geratenen Nase hätte sie eine attraktive Frau sein können, aber ihre gelben Zähne zerstörten diese Illusion.

»Kennen wir uns?«, fragte Nik.

»Ich kenne Sie«, antwortete die Frau ruhig. »Sie haben mir in den letzten Tagen viel Ärger beschert.«

»Weil ich Ihre Verbindung zur Kripo getötet habe?«, fragte Nik.

»Herr Hübner war nicht unsere einzige Verbindung«, widersprach sie, »aber eine unserer besten. Zusammen mit Dr. Cüpper, die wegen Ihnen untertauchen musste, entstand eine Lücke.«

»Nicht zu vergessen die Schließung der Klinik.«

»Damit haben wir nichts zu tun«, sagte sie lächelnd.

»Sie waren das mit Roswitha?«, stellte Nik fest. »Sie haben das junge Mädchen zum Sterben in mein Treppenhaus gehängt.«

Sie zuckte teilnahmslos die Achseln.

»Wenn Roswitha wieder ansprechbar ist, wird sie mir sagen, wer ihr das angetan hat, und wenn Sie dabei eine Rolle spielen, komme ich Sie holen. Ich verspreche, dass es kein leichter Tod wird.«

Sie lachte. Es war eine Mischung aus Überraschung und Arroganz, als hegte sie keinen Zweifel, wer als Sieger aus dieser Begegnung herauskommen würde. »Sie hätten die Fälle ruhen lassen sollen. Niemand wäre zu Schaden gekommen. Ihr Kollege würde noch leben und ich müsste Ihnen keine Kugel verpassen.« Sie wandte sich an Verbeck. »Und du machst, dass du abhaust. Nicht dass du den Boden vollkotzt, weil du kein Blut sehen kannst.«

Verbeck nickte und setzte sich mit gesenktem Kopf in Bewegung.

Nik wägte seine Optionen ab. Die Frau war zu weit weg, als dass er ihre Pistole hätte greifen können. Sie wirkte ruhig, und ihre Hände zitterten nicht. Sie schien in solchen Dingen erfahren zu sein, also war sie beim Militär gewesen oder stammte aus der Sicherheitsbranche. Niemand, der die Nerven verlor, wenn es darauf ankam. Zum Gang war es nur einen Schritt nach hinten. Ein Sprung hätte ihn näher ans Treppenhaus und aus der Schussbahn gebracht, aber Nik hatte bei dem Gespräch das Knirschen von Plastikfolie gehört, also wartete eine zweite Person am Eingang, sicherlich auch mit einer SIG in den Händen.

In Niks Kopf formte sich ein Plan, der dumm, gefährlich und mit Schmerzen verbunden war, doch seine einzige Chance, den Ort lebend verlassen zu können. Er seufzte und wartete, bis Verbeck die Tür erreicht hatte. Dort packte er den Junkie an den Schultern und stieß ihn in Richtung der blonden Frau. Verbeck fiel mit ausgestreckten Armen auf sie. Die Schussbahn war verstellt und sie brauchte einen Moment, um Verbeck zur Seite zu schieben. Die Zeit war ausreichend. Nik rannte zum Erker, nahm den Kopf zwischen die Arme, schloss die Augen und sprang durch das geschlossene Fenster. Glücklicherweise bestand es nicht aus Sicherheitsglas, sonst wäre es eine kurze Flucht geworden. Wenn er sich nicht getäuscht hatte, würde er bei diesem Absprungwinkel den Container knapp verfehlen und im verschneiten Gras des Vorgartens landen.

Der Aufprall war schmerzhaft. Er hatte zu viel Schwung, sodass er sich mit der Schulter abrollen musste, was ihn durch einen Kirschlorbeer katapultierte. Er ignorierte die Schmerzen und rollte weiter zu dem Container. Ein Schuss ertönte. Dann ein zweiter, dumpfer, lauter, aus einer größeren Pistole. Er hatte sich bezüglich des zweiten Manns nicht getäuscht. Er spürte das Zischen neben seinem Kopf. Zu Boden gepresst, robbte er sich weiter zum Zaun. Der Container war hoch genug, dass ihn die Frau nicht aus dem Fenster im ersten Stock erschießen konnte. Seine Hose war am rechten Knie zerrissen und Blut lief an seinem linken Arm hinunter. Wahrscheinlich hatte sich eine Scherbe in die Haut gebohrt, aber er hatte keine Zeit, sich darum zu kümmern. Er robbte weiter, stand am Gehweg auf und humpelte bis auf die Hauptstraße. Ein stechender Schmerz zog in sein Knie, aber er musste aus der Seitengasse heraus. Er zog seine Jacke aus und legte sie sich über den blutenden Arm. An der Kreuzung bemerkte er einen Aufsteller, der das Mittagsbüfett eines Wirtshauses anpries. Er öffnete die Tür, nickte der Bedienung freundlich zu und setzte sich an einen kleinen Tisch neben eine Seniorengruppe, die gerade aufstand, um sich über das Büfett herzumachen. Nik ballte die Fäuste unter dem Tisch in dem Versuch, seine Schmerzen zu verbergen. Glücklicherweise war das gegrillte Fleisch auf dem Büfett zu verlockend, als dass ein Besucher von ihm Notiz nahm. Er saß mit dem Rücken zur Wand, den Blick auf den Eingang gerichtet, aber weder Verbeck noch die blonde Frau zeigten sich.

Er war entkommen. Für den Augenblick.





