
import json
import os


def clean_word(word):
    return str(word).strip().lower()

def load_audible_sync(file_path):
    file_lines = open(file_path).read().splitlines()

    words = []
    for line in file_lines:
        parts = line.split(",")

        word = {
            "start": int(parts[0]),
            "end": int(parts[1]),
            "word": clean_word("".join(parts[2:]))
        }
        word["start_str"] = int_time_to_str(word["start"])
        words.append(word)

    return words

def float_time_to_ms(float_time):
    return int(float_time*1000)

def int_time_to_str(int_time):
    t = int_time
    return "%dh%dm%ds%dms" % ((t/1000/60/60) % 60, (t/1000/60) % 60, (t/1000) % 60, t % 1000)

def load_ds(folder_path):
    words = []

    for ds_file_name in sorted(os.listdir(folder_path)):
        if not ds_file_name.endswith(".json"):
            print("Reading only json files from the folder. File %s not json file" % ds_file_name)
            continue

        with open(os.path.join(folder_path, ds_file_name)) as ds_file:
            print("Reading deepSpeech file: %s" % ds_file_name)
            json_data = json.load(ds_file)

            if "start_time" in json_data:
                chapter_start_time = float_time_to_ms(json_data["start_time"])
            else:
                chapter_start_time = 0
            print("Chapter start: " + str(chapter_start_time))

            for ds_word in json_data["words"]:
                word = {
                    "start": chapter_start_time + float_time_to_ms(ds_word["start_time "]),
                    "duration": float_time_to_ms(ds_word["duration"]),
                    "word": ds_word["word"]
                }
                word["start_str"] = int_time_to_str(word["start"])
                words.append(word)

    return words


if __name__ == '__main__':
    AUDIBLE_FILE = "../files/audible/Wenn die Zeit gekommen ist (Ein Jan-Tommen - Hartung, Alexander1-6.calibre.txt.csvsync"
    audible = load_audible_sync(AUDIBLE_FILE)
    print(json.dumps(audible[:10], indent=4))
    print(json.dumps(audible[-10:], indent=4))
    open("audible.json", "w").write(json.dumps(audible, indent=2))

    DS_FOLDER = "../files/DeepSpeech/3wdzgi_1-6"
    ds = load_ds(DS_FOLDER)
    print(json.dumps(ds[:10], indent=4))
    print(json.dumps(ds[-10:], indent=4))
    open("ds.json", "w").write(json.dumps(ds, indent=2))


