# Opis projekta se nahaja v datoteki **report.pdf**

# ONJ project #

The goal of this project is to create a tool to automatically synchornize audio and text. This is also often refered as "foreced aligment". 
The main langauge we are going to try to support is German. Additionaly we might also include English and Japanese. 

**Audio files** are downlodable form a shared google drive folder: https://drive.google.com/open?id=1NfGMMZymfifsj1kBfA2WZVtGv5XG4K9d 


### Existing solutions ###

The list of all the **open sourced** solutions can be found here:
https://github.com/pettarin/forced-alignment-tools#programs-and-libraries

* Kaldi: [Git](http://kaldi-asr.org/index.html) [Site](http://kaldi-asr.org/index.html)
* Espresso [Git](https://github.com/freewym/espresso) [Paper](https://deepai.org/publication/espresso-a-fast-end-to-end-neural-speech-recognition-toolkit)
* EESEN [Git](https://github.com/srvk/eesen) [Paper](https://arxiv.org/abs/1507.08240) 
* Wav2Letter [Git](https://github.com/facebookresearch/wav2letter) [Paper](https://arxiv.org/abs/1609.03193)
* NeMo [Git](https://github.com/NVIDIA/NeMo)

### Source materials ###

The audio it's corresponding text was obtained from various audio books that I have in my personal library. 
We will test our algorithm on already matched dataset like this one: https://www.readbeyond.it/ebooks.html

### Approach ###

Using existing learned network for ASR (like Kaldi or something newer) and then matching generated text to the transcript.
In best case scenario we will train our own network to do the ASR



### Team members ###

* An�e Bertoncelj
* Tadej Vatovani
